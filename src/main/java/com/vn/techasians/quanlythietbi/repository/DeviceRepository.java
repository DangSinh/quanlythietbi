package com.vn.techasians.quanlythietbi.repository;

import com.vn.techasians.quanlythietbi.domain.Device;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Device entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DeviceRepository extends JpaRepository<Device, Long> {
    @Query("SELECT e FROM Device e")
    Page<Device> findDevices(Pageable pageable);
}
