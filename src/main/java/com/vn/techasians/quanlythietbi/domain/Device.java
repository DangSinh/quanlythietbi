package com.vn.techasians.quanlythietbi.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;

/**
 * A Device.
 */
@Entity
@Table(name = "device")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Device implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name_device")
    private String nameDevice;

    @Column(name = "code_device")
    private String codeDevice;

    @Column(name = "producer")
    private String producer;

    @Column(name = "code_producer")
    private String codeProducer;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "status")
    private Long status;

    @Column(name = "start_date_im")
    private Instant startDateIm;

    @Column(name = "start_user_use")
    private Instant startUserUse;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameDevice() {
        return nameDevice;
    }

    public Device nameDevice(String nameDevice) {
        this.nameDevice = nameDevice;
        return this;
    }

    public void setNameDevice(String nameDevice) {
        this.nameDevice = nameDevice;
    }

    public String getCodeDevice() {
        return codeDevice;
    }

    public Device codeDevice(String codeDevice) {
        this.codeDevice = codeDevice;
        return this;
    }

    public void setCodeDevice(String codeDevice) {
        this.codeDevice = codeDevice;
    }

    public String getProducer() {
        return producer;
    }

    public Device producer(String producer) {
        this.producer = producer;
        return this;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getCodeProducer() {
        return codeProducer;
    }

    public Device codeProducer(String codeProducer) {
        this.codeProducer = codeProducer;
        return this;
    }

    public void setCodeProducer(String codeProducer) {
        this.codeProducer = codeProducer;
    }

    public Long getUserId() {
        return userId;
    }

    public Device userId(Long userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getStatus() {
        return status;
    }

    public Device status(Long status) {
        this.status = status;
        return this;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Instant getStartDateIm() {
        return startDateIm;
    }

    public Device startDateIm(Instant startDateIm) {
        this.startDateIm = startDateIm;
        return this;
    }

    public void setStartDateIm(Instant startDateIm) {
        this.startDateIm = startDateIm;
    }

    public Instant getStartUserUse() {
        return startUserUse;
    }

    public Device startUserUse(Instant startUserUse) {
        this.startUserUse = startUserUse;
        return this;
    }

    public void setStartUserUse(Instant startUserUse) {
        this.startUserUse = startUserUse;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Device)) {
            return false;
        }
        return id != null && id.equals(((Device) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Device{" +
            "id=" + getId() +
            ", nameDevice='" + getNameDevice() + "'" +
            ", codeDevice='" + getCodeDevice() + "'" +
            ", producer='" + getProducer() + "'" +
            ", codeProducer='" + getCodeProducer() + "'" +
            ", userId=" + getUserId() +
            ", status=" + getStatus() +
            ", startDateIm='" + getStartDateIm() + "'" +
            ", startUserUse='" + getStartUserUse() + "'" +
            "}";
    }
}
