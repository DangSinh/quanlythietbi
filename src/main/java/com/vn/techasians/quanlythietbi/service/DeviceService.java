package com.vn.techasians.quanlythietbi.service;

import com.vn.techasians.quanlythietbi.domain.Device;
import com.vn.techasians.quanlythietbi.repository.DeviceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class DeviceService {
    private final Logger log = LoggerFactory.getLogger(DeviceService.class);

    private final DeviceRepository deviceRepository;

    public DeviceService(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    public Page<Device> getListDevice() {
        Pageable pageable = PageRequest.of(0, 10);
        return this.deviceRepository.findDevices(pageable);
    }
}
