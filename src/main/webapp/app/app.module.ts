import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import './vendor';
import { QuanlythietbiSharedModule } from 'app/shared/shared.module';
import { QuanlythietbiCoreModule } from 'app/core/core.module';
import { QuanlythietbiAppRoutingModule } from './app-routing.module';
import { QuanlythietbiHomeModule } from './home/home.module';
import { QuanlythietbiEntityModule } from './entities/entity.module';
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuComponent } from './layouts/menu/menu.component';
import { TooltipModule } from 'primeng';

@NgModule({
  imports: [
    BrowserModule,
    QuanlythietbiSharedModule,
    QuanlythietbiCoreModule,
    QuanlythietbiHomeModule,
    QuanlythietbiEntityModule,
    QuanlythietbiAppRoutingModule,
    BrowserAnimationsModule,
    TooltipModule
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent, MenuComponent],
  exports: [],
  bootstrap: [MainComponent]
})
export class QuanlythietbiAppModule {}
