export const COLUMNS = [
  {
    id: 1,
    field: 'suggestCode',
    header: 'concavepoint.label.suggestCode',
    classField: 'text-center',
    styleWidth: '150px',
    bodyClassField: 'text-left'
  },
  {
    id: 2,
    field: 'suggestTypeStr',
    header: 'station.management.label.typeSuggestion',
    classField: 'text-center',
    styleWidth: '150px',
    bodyClassField: 'text-left'
  },
  {
    id: 3,
    field: 'suggestStatusStr',
    header: 'station.management.label.statusSuggest',
    classField: 'text-center',
    styleWidth: '150px',
    bodyClassField: 'text-left'
  },
  {
    id: 4,
    field: 'deptName',
    header: 'station.management.label.unitSuggest',
    classField: 'text-center',
    styleWidth: '150px',
    bodyClassField: 'text-left'
  },
  {
    id: 5,
    field: 'userSearch',
    header: 'station.management.label.personSuggest',
    classField: 'text-center',
    styleWidth: '150px',
    bodyClassField: 'text-left'
  },
  {
    id: 6,
    field: 'createTimeStr',
    header: 'device.equip.createDate',
    classField: 'text-center',
    styleWidth: '150px',
    bodyClassField: 'text-center'
  }
];
export const SUGGEST_TYPES = [
  { label: '--Lựa chọn--', value: null },
  { value: 0, label: 'Đề xuất mới' },
  { value: 1, label: 'Đề xuất cosite' },
  { value: 2, label: 'Đề xuất di dời' },
  { value: 3, label: 'Đề xuất hủy' },
  { value: 4, label: 'Đề xuất nâng hạ độ cao anten' },
  { value: 5, label: 'Đề xuất thêm cell' },
  { value: 6, label: 'Đề xuất swap thiết bị' },
  { value: 7, label: 'Đề xuất đổi mã' }
];
