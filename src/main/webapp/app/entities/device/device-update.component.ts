import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IDevice, Device } from 'app/shared/model/device.model';
import { DeviceService } from './device.service';

@Component({
  selector: 'jhi-device-update',
  templateUrl: './device-update.component.html'
})
export class DeviceUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    nameDevice: [],
    codeDevice: [],
    producer: [],
    codeProducer: [],
    userId: [],
    status: [],
    startDateIm: [],
    startUserUse: []
  });

  constructor(protected deviceService: DeviceService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ device }) => {
      if (!device.id) {
        const today = moment().startOf('day');
        device.startDateIm = today;
        device.startUserUse = today;
      }

      this.updateForm(device);
    });
  }

  updateForm(device: IDevice): void {
    this.editForm.patchValue({
      id: device.id,
      nameDevice: device.nameDevice,
      codeDevice: device.codeDevice,
      producer: device.producer,
      codeProducer: device.codeProducer,
      userId: device.userId,
      status: device.status,
      startDateIm: device.startDateIm ? device.startDateIm.format(DATE_TIME_FORMAT) : null,
      startUserUse: device.startUserUse ? device.startUserUse.format(DATE_TIME_FORMAT) : null
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const device = this.createFromForm();
    if (device.id !== undefined) {
      this.subscribeToSaveResponse(this.deviceService.update(device));
    } else {
      this.subscribeToSaveResponse(this.deviceService.create(device));
    }
  }

  private createFromForm(): IDevice {
    return {
      ...new Device(),
      id: this.editForm.get(['id'])!.value,
      nameDevice: this.editForm.get(['nameDevice'])!.value,
      codeDevice: this.editForm.get(['codeDevice'])!.value,
      producer: this.editForm.get(['producer'])!.value,
      codeProducer: this.editForm.get(['codeProducer'])!.value,
      userId: this.editForm.get(['userId'])!.value,
      status: this.editForm.get(['status'])!.value,
      startDateIm: this.editForm.get(['startDateIm'])!.value
        ? moment(this.editForm.get(['startDateIm'])!.value, DATE_TIME_FORMAT)
        : undefined,
      startUserUse: this.editForm.get(['startUserUse'])!.value
        ? moment(this.editForm.get(['startUserUse'])!.value, DATE_TIME_FORMAT)
        : undefined
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDevice>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
