import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IDevice } from 'app/shared/model/device.model';
import { DeviceService } from './device.service';
import { DeviceDeleteDialogComponent } from './device-delete-dialog.component';
import { COLUMNS } from 'app/entities/device/constant/table-device-header.constant';
import { MessageService, Table } from 'primeng';

@Component({
  selector: 'jhi-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.scss'],
  providers: [MessageService]
})
export class DeviceComponent implements OnInit, OnDestroy {
  devices?: any[];
  eventSubscriber?: Subscription;
  contextMenuSelection: any;
  selectedRow: any;
  columns = COLUMNS;
  @ViewChild('dt') dt: any;
  selectedColumns = [
    { field: 'nameDevice', header: 'Tên thiết bị', classField: 'text-center' },
    { field: 'codeDevice', header: 'Mã thiết bị', classField: 'text-center' },
    { field: 'producer', header: 'Nhà sản xuất', classField: 'text-center' },
    { field: 'codeProducer', header: 'Mã nhà sản xuất', classField: 'text-center' },
    { field: 'userId', header: 'Người sở hữu', classField: 'text-center' },
    { field: 'status', header: 'Trạng thái', classField: 'text-center' },
    { field: 'startDateIm', header: 'Thời gian nhập', classField: 'text-center' },
    { field: 'startUserUse', header: 'Thời gian sử dụng', classField: 'text-center' }
  ];

  constructor(protected deviceService: DeviceService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInDevices();
  }

  loadAll(): void {
    this.deviceService.query().subscribe(res => {
      this.devices = res.body;
    });
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IDevice): number {
    return item.id!;
  }

  registerChangeInDevices(): void {
    this.eventSubscriber = this.eventManager.subscribe('deviceListModification', () => this.loadAll());
  }

  delete(device: IDevice): void {
    const modalRef = this.modalService.open(DeviceDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.device = device;
  }

  loadLazy($event: any) {}
}
