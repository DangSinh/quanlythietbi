import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { QuanlythietbiSharedModule } from 'app/shared/shared.module';
import { DeviceComponent } from './device.component';
import { DeviceDetailComponent } from './device-detail.component';
import { DeviceUpdateComponent } from './device-update.component';
import { DeviceDeleteDialogComponent } from './device-delete-dialog.component';
import { deviceRoute } from './device.route';

import {
  ButtonModule,
  ContextMenuModule,
  DialogModule,
  DropdownModule,
  PaginatorModule,
  ScrollPanelModule,
  TableModule,
  TooltipModule
} from 'primeng';

@NgModule({
  imports: [
    QuanlythietbiSharedModule,
    RouterModule.forChild(deviceRoute),
    ButtonModule,
    DialogModule,
    ScrollPanelModule,
    TableModule,
    TooltipModule,
    DropdownModule,
    ContextMenuModule,
    PaginatorModule
  ],
  declarations: [DeviceComponent, DeviceDetailComponent, DeviceUpdateComponent, DeviceDeleteDialogComponent],
  entryComponents: [DeviceDeleteDialogComponent]
})
export class QuanlythietbiDeviceModule {}
