import { Component, OnInit } from '@angular/core';
import { AccountService } from 'app/core/auth/account.service';
import { LoginService } from 'app/core/login/login.service';
import { LoginModalService } from 'app/core/login/login-modal.service';
import { Router } from '@angular/router';

@Component({
  selector: 'jhi-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  isClose = false;

  constructor(
    private accountService: AccountService,
    private loginService: LoginService,
    private loginModalService: LoginModalService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  toggleNavigation(): void {
    this.isClose = !this.isClose;
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  logout(): void {
    this.loginService.logout();
    this.router.navigate(['']);
  }
}
