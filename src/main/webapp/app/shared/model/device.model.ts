import { Moment } from 'moment';

export interface IDevice {
  id?: number;
  nameDevice?: string;
  codeDevice?: string;
  producer?: string;
  codeProducer?: string;
  userId?: number;
  status?: number;
  startDateIm?: Moment;
  startUserUse?: Moment;
}

export class Device implements IDevice {
  constructor(
    public id?: number,
    public nameDevice?: string,
    public codeDevice?: string,
    public producer?: string,
    public codeProducer?: string,
    public userId?: number,
    public status?: number,
    public startDateIm?: Moment,
    public startUserUse?: Moment
  ) {}
}
