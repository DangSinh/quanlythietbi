import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ContentChild,
  ElementRef,
  HostBinding,
  HostListener,
  Input,
  OnInit,
  Renderer2,
  ViewChild
} from '@angular/core';
import { MessageService, Table, TableHeaderCheckbox } from 'primeng';

@Component({
  selector: 'jhi-ta-table',
  templateUrl: './ta-table.component.html',
  styleUrls: ['./ta-table.component.scss']
})
export class TaTableComponent implements OnInit, AfterViewInit {
  rowsPerPageOptions = [5, 10, 15, 20, 25, 30];
  scrollHeight = '280px';
  // isViewFull = true;
  @ContentChild(Table) table: any;
  @ViewChild('cm') cm: any;
  @ViewChild(TableHeaderCheckbox) tableHeaderCheckbox: any;
  @Input() contextMenuSelection: any;
  @Input() frozenWidth: any;
  @HostBinding('class.view-full') isViewFull = false;
  @HostBinding('class.frozen-column') @Input() isFrozen = false;

  contextItems: any;
  contextItemsFixed = [
    // {
    //   label: this.showLabelOption(),
    //   icon: 'fa fa-expand',
    //   command: () => {
    //     this.isViewFull = !this.isViewFull;
    //     this.contextItems[0].label = this.showLabelOption();
    //   }
    // },
    {
      label: 'Copy',
      icon: 'fa fa-clipboard',
      command: () => {
        this.copyRow(this.contextMenuSelection);
        this.messageService.add({
          key: 'toastSuccess',
          severity: 'success',
          detail: 'Đã sao chép dữ liệu'
        });
      }
    }
  ];

  constructor(
    private messageService: MessageService,
    private cdRef: ChangeDetectorRef,
    private elementRef: ElementRef,
    private renderer: Renderer2
  ) {}

  @HostListener('contextmenu', ['$event']) onContextmenu(event: any) {
    if (
      event.target.tagName.toString().toLowerCase() === 'th' ||
      event.target.parentElement.tagName
        .toString()
        .toString()
        .toLowerCase() === 'th'
    ) {
      this.contextItems = [...[this.contextItemsFixed[0]]];
    } else {
      this.contextItems = [...this.contextItemsFixed];
    }
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    if (this.isFrozen) {
      this.renderer.setStyle(this.elementRef.nativeElement.querySelector('table'), 'margin-left', this.frozenWidth);
    }
  }

  copyRow(data: any): void {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = data;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  showLabelOption(): string {
    return this.isViewFull ? 'Hiển thị một phần' : 'Hiển thị đầy đủ';
  }

  isDisabledTooltip(td: HTMLElement): any {
    return this.isViewFull || td.offsetWidth > td.scrollWidth;
  }

  onTableHeaderCheckboxToggle(event: any): any {
    if (event.checked === true) {
      this.table.selection = this.table.value.filter(
        (item: any, i: any) => i >= this.table.first && i < this.table.first + this.table.rows
      );
      // }
    } else {
      this.table.selection = [];
    }
    return this.table.selection;
  }

  onPageChange(): any {
    this.table.selection = [];
    return this.table.selection;
  }

  onRowSelect() {
    if (this.table.lazy) {
      return;
    }
    if (this.table.selection === this.table.rows) {
      this.tableHeaderCheckbox.checked = true;
      this.tableHeaderCheckbox.updateCheckedState();
    }
    return this.table.selection;
  }
}
