import { NgModule } from '@angular/core';
import { QuanlythietbiSharedLibsModule } from './shared-libs.module';
import { FindLanguageFromKeyPipe } from './language/find-language-from-key.pipe';
import { AlertComponent } from './alert/alert.component';
import { AlertErrorComponent } from './alert/alert-error.component';
import { LoginModalComponent } from './login/login.component';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';
import { TaTableComponent } from 'app/shared/ta-table/ta-table.component';
import { ContextMenuModule } from 'primeng';

@NgModule({
  imports: [QuanlythietbiSharedLibsModule, ContextMenuModule],
  declarations: [
    FindLanguageFromKeyPipe,
    AlertComponent,
    AlertErrorComponent,
    LoginModalComponent,
    HasAnyAuthorityDirective,
    TaTableComponent
  ],
  entryComponents: [LoginModalComponent],
  exports: [
    QuanlythietbiSharedLibsModule,
    FindLanguageFromKeyPipe,
    AlertComponent,
    AlertErrorComponent,
    LoginModalComponent,
    HasAnyAuthorityDirective,
    TaTableComponent
  ]
})
export class QuanlythietbiSharedModule {}
