package com.vn.techasians.quanlythietbi.web.rest;

import com.vn.techasians.quanlythietbi.QuanlythietbiApp;
import com.vn.techasians.quanlythietbi.domain.Device;
import com.vn.techasians.quanlythietbi.repository.DeviceRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DeviceResource} REST controller.
 */
@SpringBootTest(classes = QuanlythietbiApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class DeviceResourceIT {

    private static final String DEFAULT_NAME_DEVICE = "AAAAAAAAAA";
    private static final String UPDATED_NAME_DEVICE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE_DEVICE = "AAAAAAAAAA";
    private static final String UPDATED_CODE_DEVICE = "BBBBBBBBBB";

    private static final String DEFAULT_PRODUCER = "AAAAAAAAAA";
    private static final String UPDATED_PRODUCER = "BBBBBBBBBB";

    private static final String DEFAULT_CODE_PRODUCER = "AAAAAAAAAA";
    private static final String UPDATED_CODE_PRODUCER = "BBBBBBBBBB";

    private static final Long DEFAULT_USER_ID = 1L;
    private static final Long UPDATED_USER_ID = 2L;

    private static final Long DEFAULT_STATUS = 1L;
    private static final Long UPDATED_STATUS = 2L;

    private static final Instant DEFAULT_START_DATE_IM = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_START_DATE_IM = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_START_USER_USE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_START_USER_USE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDeviceMockMvc;

    private Device device;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Device createEntity(EntityManager em) {
        Device device = new Device()
            .nameDevice(DEFAULT_NAME_DEVICE)
            .codeDevice(DEFAULT_CODE_DEVICE)
            .producer(DEFAULT_PRODUCER)
            .codeProducer(DEFAULT_CODE_PRODUCER)
            .userId(DEFAULT_USER_ID)
            .status(DEFAULT_STATUS)
            .startDateIm(DEFAULT_START_DATE_IM)
            .startUserUse(DEFAULT_START_USER_USE);
        return device;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Device createUpdatedEntity(EntityManager em) {
        Device device = new Device()
            .nameDevice(UPDATED_NAME_DEVICE)
            .codeDevice(UPDATED_CODE_DEVICE)
            .producer(UPDATED_PRODUCER)
            .codeProducer(UPDATED_CODE_PRODUCER)
            .userId(UPDATED_USER_ID)
            .status(UPDATED_STATUS)
            .startDateIm(UPDATED_START_DATE_IM)
            .startUserUse(UPDATED_START_USER_USE);
        return device;
    }

    @BeforeEach
    public void initTest() {
        device = createEntity(em);
    }

    @Test
    @Transactional
    public void createDevice() throws Exception {
        int databaseSizeBeforeCreate = deviceRepository.findAll().size();

        // Create the Device
        restDeviceMockMvc.perform(post("/api/devices").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(device)))
            .andExpect(status().isCreated());

        // Validate the Device in the database
        List<Device> deviceList = deviceRepository.findAll();
        assertThat(deviceList).hasSize(databaseSizeBeforeCreate + 1);
        Device testDevice = deviceList.get(deviceList.size() - 1);
        assertThat(testDevice.getNameDevice()).isEqualTo(DEFAULT_NAME_DEVICE);
        assertThat(testDevice.getCodeDevice()).isEqualTo(DEFAULT_CODE_DEVICE);
        assertThat(testDevice.getProducer()).isEqualTo(DEFAULT_PRODUCER);
        assertThat(testDevice.getCodeProducer()).isEqualTo(DEFAULT_CODE_PRODUCER);
        assertThat(testDevice.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testDevice.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testDevice.getStartDateIm()).isEqualTo(DEFAULT_START_DATE_IM);
        assertThat(testDevice.getStartUserUse()).isEqualTo(DEFAULT_START_USER_USE);
    }

    @Test
    @Transactional
    public void createDeviceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = deviceRepository.findAll().size();

        // Create the Device with an existing ID
        device.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDeviceMockMvc.perform(post("/api/devices").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(device)))
            .andExpect(status().isBadRequest());

        // Validate the Device in the database
        List<Device> deviceList = deviceRepository.findAll();
        assertThat(deviceList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDevices() throws Exception {
        // Initialize the database
        deviceRepository.saveAndFlush(device);

        // Get all the deviceList
        restDeviceMockMvc.perform(get("/api/devices?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(device.getId().intValue())))
            .andExpect(jsonPath("$.[*].nameDevice").value(hasItem(DEFAULT_NAME_DEVICE)))
            .andExpect(jsonPath("$.[*].codeDevice").value(hasItem(DEFAULT_CODE_DEVICE)))
            .andExpect(jsonPath("$.[*].producer").value(hasItem(DEFAULT_PRODUCER)))
            .andExpect(jsonPath("$.[*].codeProducer").value(hasItem(DEFAULT_CODE_PRODUCER)))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.intValue())))
            .andExpect(jsonPath("$.[*].startDateIm").value(hasItem(DEFAULT_START_DATE_IM.toString())))
            .andExpect(jsonPath("$.[*].startUserUse").value(hasItem(DEFAULT_START_USER_USE.toString())));
    }
    
    @Test
    @Transactional
    public void getDevice() throws Exception {
        // Initialize the database
        deviceRepository.saveAndFlush(device);

        // Get the device
        restDeviceMockMvc.perform(get("/api/devices/{id}", device.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(device.getId().intValue()))
            .andExpect(jsonPath("$.nameDevice").value(DEFAULT_NAME_DEVICE))
            .andExpect(jsonPath("$.codeDevice").value(DEFAULT_CODE_DEVICE))
            .andExpect(jsonPath("$.producer").value(DEFAULT_PRODUCER))
            .andExpect(jsonPath("$.codeProducer").value(DEFAULT_CODE_PRODUCER))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID.intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.intValue()))
            .andExpect(jsonPath("$.startDateIm").value(DEFAULT_START_DATE_IM.toString()))
            .andExpect(jsonPath("$.startUserUse").value(DEFAULT_START_USER_USE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDevice() throws Exception {
        // Get the device
        restDeviceMockMvc.perform(get("/api/devices/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDevice() throws Exception {
        // Initialize the database
        deviceRepository.saveAndFlush(device);

        int databaseSizeBeforeUpdate = deviceRepository.findAll().size();

        // Update the device
        Device updatedDevice = deviceRepository.findById(device.getId()).get();
        // Disconnect from session so that the updates on updatedDevice are not directly saved in db
        em.detach(updatedDevice);
        updatedDevice
            .nameDevice(UPDATED_NAME_DEVICE)
            .codeDevice(UPDATED_CODE_DEVICE)
            .producer(UPDATED_PRODUCER)
            .codeProducer(UPDATED_CODE_PRODUCER)
            .userId(UPDATED_USER_ID)
            .status(UPDATED_STATUS)
            .startDateIm(UPDATED_START_DATE_IM)
            .startUserUse(UPDATED_START_USER_USE);

        restDeviceMockMvc.perform(put("/api/devices").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedDevice)))
            .andExpect(status().isOk());

        // Validate the Device in the database
        List<Device> deviceList = deviceRepository.findAll();
        assertThat(deviceList).hasSize(databaseSizeBeforeUpdate);
        Device testDevice = deviceList.get(deviceList.size() - 1);
        assertThat(testDevice.getNameDevice()).isEqualTo(UPDATED_NAME_DEVICE);
        assertThat(testDevice.getCodeDevice()).isEqualTo(UPDATED_CODE_DEVICE);
        assertThat(testDevice.getProducer()).isEqualTo(UPDATED_PRODUCER);
        assertThat(testDevice.getCodeProducer()).isEqualTo(UPDATED_CODE_PRODUCER);
        assertThat(testDevice.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testDevice.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testDevice.getStartDateIm()).isEqualTo(UPDATED_START_DATE_IM);
        assertThat(testDevice.getStartUserUse()).isEqualTo(UPDATED_START_USER_USE);
    }

    @Test
    @Transactional
    public void updateNonExistingDevice() throws Exception {
        int databaseSizeBeforeUpdate = deviceRepository.findAll().size();

        // Create the Device

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDeviceMockMvc.perform(put("/api/devices").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(device)))
            .andExpect(status().isBadRequest());

        // Validate the Device in the database
        List<Device> deviceList = deviceRepository.findAll();
        assertThat(deviceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDevice() throws Exception {
        // Initialize the database
        deviceRepository.saveAndFlush(device);

        int databaseSizeBeforeDelete = deviceRepository.findAll().size();

        // Delete the device
        restDeviceMockMvc.perform(delete("/api/devices/{id}", device.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Device> deviceList = deviceRepository.findAll();
        assertThat(deviceList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
