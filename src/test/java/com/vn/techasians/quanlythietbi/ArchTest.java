package com.vn.techasians.quanlythietbi;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.vn.techasians.quanlythietbi");

        noClasses()
            .that()
                .resideInAnyPackage("com.vn.techasians.quanlythietbi.service..")
            .or()
                .resideInAnyPackage("com.vn.techasians.quanlythietbi.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..com.vn.techasians.quanlythietbi.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
